# README

### How to run this demo

- To view this demo open the index.html in a browser

### Notes

- I mostly made use of ES6 syntax, therefore I did not take older browsers into consideration for this demo
- I treated all links as mock links

### TODOs

- Better form validation, especially with regards to the dates
- Add more structure to JavaScript code
- Add UI library for cross browser consistency and compatibility
- Add build tools to optimize and minify scripts and css
