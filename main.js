const backgroundImages = ['./assets/images/1.jpg', './assets/images/2.jpg', './assets/images/3.jpg'];

const backgrounds = backgroundImages.map((src) => {
  const img = new Image();
  img.src = src;
  return img;
});

window.onload = () => {
  const body = document.querySelector('body');
  const searchButton = document.querySelector('button[type="submit"]');
  const searchForm = document.querySelector('form.search-form');
  const toggleButtons = document.querySelectorAll('.toggle-button');
  const flightType = document.querySelector('#flightType');
  const bookingOptionsContainer = document.querySelector('#bookingOptionsContainer');
  const contactLinks = document.querySelector('#contactLinks');
  const headerLinksContainer = document.querySelector('#headerLinksContainer');
  const contactLinksMobileTrigger = document.querySelector('#contactLinksMobileTrigger');
  const requiredFields = searchForm.querySelectorAll('[required]');
  let backgroundCount = 1;

  const bookingOptions = [
    { displayName: 'Flights', url: '#', icon: 'airplane', promotion: null },
    { displayName: 'Hotels', url: '#', icon: 'hotel', promotion: null },
    { displayName: 'Flights + Hotels', url: '#', icon: 'pricetag', promotion: 'save' },
    { displayName: 'Holidays', url: '#', icon: 'gift', promotion: null }
  ];

  const socialLinks = [
    { icon: 'facebook', url: '#' },
    { icon: 'twitter', url: '#' },
    { icon: 'pinterest', url: '#' },
    { icon: 'instagram', url: '#' },
    { icon: 'phone', url: '#' },
    { icon: 'mail', url: '#' }
  ];

  const headerLinks = [
    { displayName: 'HOME', url: '#', icon: 'home' },
    { displayName: 'FLIGHTS', url: '#', icon: 'airplane' },
    { displayName: 'HOTELS', url: '#', icon: 'hotel' },
    { displayName: 'HOLIDAYS', url: '#', icon: 'gift' },
    { displayName: 'BLOG', url: '#', icon: 'pencil' },
    { displayName: '1800 105 2541', url: '#', icon: 'phone' }
  ];

  initBackgroundRotate();

  // EVENT LISTENERS

  // validation
  requiredFields.forEach((field) => {
    field.addEventListener('change', validateInputFields);
    field.addEventListener('keyup', validateInputFields);
  });

  // form submission
  searchButton.addEventListener('click', (event) => submit(event));

  // toggle flight type
  toggleButtons.forEach((button) =>
    button.addEventListener('click', () => {
      toggleActive(button);
      flightType.value = button.dataset.type;
    })
  );

  // show hide contact icon links based on browser size
  contactLinksMobileTrigger.addEventListener('mouseover', (event) => contactLinks.classList.add('show-mobile'));

  contactLinksMobileTrigger.addEventListener('click', (event) => {
    contactLinks.classList.toggle('show-mobile');
    for (const child of contactLinks.children) {
      child.addEventListener('click', (event) => {
        contactLinks.classList.remove('show-mobile');
      });
    }
  });

  contactLinksMobileTrigger.addEventListener('blur', (event) => {
    contactLinks.classList.toggle('show-mobile');
  });

  // FUNCTIONS

  function toggleActive(element) {
    for (const child of element.parentElement.children) {
      child.classList.remove('active');
    }
    element.classList.add('active');
  }

  function changeBackground(count) {
    setTimeout(() => {
      body.style.backgroundImage = `url(${backgrounds[count].src})`;
    }, 500);
  }

  function initBackgroundRotate() {
    body.style.backgroundImage = `url(${backgrounds[0].src})`;
    setInterval(() => {
      changeBackground(backgroundCount);
      backgroundCount < backgrounds.length - 1 ? backgroundCount++ : (backgroundCount = 0);
    }, 6000);
  }

  function getInvalidFields() {
    let invalid = [];
    for (const field of requiredFields) {
      if (!field.value) {
        invalid.push(field);
      }
    }
    return invalid;
  }

  function formHasErrors() {
    let errors = [];
    for (const field of searchForm.querySelectorAll('.error')) {
      errors.push(field);
    }
    return !!errors.length;
  }

  function setSubmitButtonDisabled() {
    if (formHasErrors() || getInvalidFields().length) {
      return searchButton.setAttribute('disabled', 'true');
    }
    searchButton.removeAttribute('disabled');
  }

  function validateInputFields(event) {
    const errorElement = event.target.parentElement.querySelector('.error');
    let errorMessage;
    const value = event.target.value;

    // date validation
    if (['departure', 'returning'].includes(event.target.name)) {
      const isValidDateFormat = /^([0-9]{2}\/[0-9]{2}\/[0-9]{4}$)/;
      const isValidDate = Date.parse(value);

      if (!isValidDateFormat.test(value)) {
        errorMessage = 'Please use the MM/DD/YYYY format';
        createInputError(event, errorElement, errorMessage);
        return;
      } else if (!isValidDate) {
        errorMessage = 'Please use a valid date';
        createInputError(event, errorElement, errorMessage);
        return;
      } else if (new Date(value) < new Date()) {
        errorMessage = 'Date must be in the future';
        createInputError(event, errorElement, errorMessage);
        return;
      }

      event.target.parentElement.querySelectorAll('.error').forEach((el) => el.remove());
    }
    if (errorElement) {
      errorElement.remove();
    }
    setSubmitButtonDisabled();
  }

  // add error
  function createInputError(event, errorElement, errorMessage) {
    const element = document.createElement('p');
    if (errorElement) {
      errorElement.innerText = errorMessage;
      setSubmitButtonDisabled();
      return;
    }
    element.classList.add('error');
    element.innerText = errorMessage;
    event.target.parentElement.appendChild(element);
    setSubmitButtonDisabled();
  }

  function submit(event) {
    event.preventDefault();
    const allErrorFields = getInvalidFields();

    for (const field of allErrorFields) {
      if (!field.value) {
        for (const child of field.parentElement.children) {
          if (child.classList.contains('error')) {
            return;
          }
        }
        const errorElement = document.createElement('p');
        errorElement.classList.add('error');
        errorElement.innerText = `The ${field.name.toUpperCase()} field is not set`;
        field.parentElement.appendChild(errorElement);
        hasErrors = true;
      } else {
        const el = field.parentElement.querySelector('.error');
        if (el) {
          el.remove();
        }
      }
    }

    if (formHasErrors()) {
      return;
    }

    const formData = new FormData(searchForm);
    const formObject = {};
    for (var data of formData.entries()) {
      formObject[data[0]] = data[1];
    }
    const { from, to, departure, returning, adults, type } = formObject;
    confirm(`
  Please confirm your ${type.toUpperCase()} flight details:
    From: ${from}.
    To: ${to}
    Departure date: ${departure}.
    Arrival date: ${returning}.
    Number of Adults: ${adults}.
  `);
  }

  // CREATE STATIC ELEMENTS

  bookingOptions.forEach((item, index) => {
    const element = document.createElement('div');
    const icon = document.createElement('img');

    icon.classList.add('tab-icon');
    if (item.icon) {
      icon.setAttribute('src', `./assets/icons/${item.icon}.svg`);
      icon.setAttribute('alt', `${item.icon}`);
    }

    element.innerHTML = `<span class="booking-options-tab-text">${item.displayName}</span>`;
    element.dataset.url = item.url;
    element.classList.add('booking-options-tab');
    element.addEventListener('click', () => toggleActive(element));

    if (index === 0) {
      element.classList.add('active');
    }
    if (item.promotion) {
      element.classList.add('promotion');
      element.dataset.promotion = item.promotion;
    }
    element.prepend(icon);
    bookingOptionsContainer.appendChild(element);
  });

  socialLinks.forEach((item) => {
    const icon = document.createElement('img');
    icon.setAttribute('src', `./assets/icons/${item.icon}.svg`);
    icon.setAttribute('alt', `${item.icon}`);
    contactLinks.appendChild(icon);
  });

  headerLinks.forEach((item, index) => {
    const element = document.createElement('div');
    const icon = document.createElement('img');
    icon.classList.add('tab-icon');
    if (item.icon) {
      icon.setAttribute('src', `./assets/icons/${item.icon}.svg`);
      icon.setAttribute('alt', `${item.icon}`);
    }
    element.innerText = item.displayName;
    element.dataset.url = item.url;
    element.classList.add('header-link');
    element.addEventListener('click', () => toggleActive(element));
    if (index === 0) {
      element.classList.add('active');
    }
    element.prepend(icon);
    headerLinksContainer.appendChild(element);
  });
};
